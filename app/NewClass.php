<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewClass extends Model
{
    protected $fillable = [
        'class_name',
    ];
}
